[![pipeline status](https://gitlab.com/MiyamotoAkira/awinterstale/badges/master/pipeline.svg)](https://gitlab.com/MiyamotoAkira/awinterstale/commits/master)


<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />All work (except the website code) is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
